#include "smartpot.h"
#include "display.h"

//Sem napísať adresu i2c modulu. V tomto prípade 0x27
LiquidCrystal_I2C  lcd(0x27, 4, 5, 6, 16, 11, 12, 13, 14, POSITIVE);

struct position{
    int x;
    int y;
}pos;

void lcd_begin(){
    lcd.begin (20, 4);   //Sem napísať veľkosť vlastného displeja. V tomto prípade 20 stĺpcov, 4 riadky
    lcd.clear();
    pos.x = 1;
    pos.y = 0;
}

void lcd_print(){
    //posun nadol
    if(digitalRead(BTN_DOWN)){
        while(digitalRead(BTN_DOWN));  //nič nerobí kým je tlačidlo stlačené
        pos.y++;
    }
    //posun nahor
    if(digitalRead(BTN_UP)){
        while(digitalRead(BTN_UP));
        pos.y--;
    }
    //posun doľava
    if(digitalRead(BTN_LEFT)){
        while(digitalRead(BTN_UP));
        pos.y = 1;
        pos.x--;
    }
    //posun vpravo
    if(digitalRead(BTN_RIGHT)){
        while(digitalRead(BTN_UP));
        pos.y = 1;
        pos.x++;
    }
    //obmedzenie pozície x
    if(pos.x < 0)
        pos.x = 2;
    if(pos.x > 2)
        pos.x = 0;
    lcd.home();
    switch (pos.x) {
        case 0:
            Screen0();
            break;
        case 1:
            Screen1();
            break;
        case 2:
            Screen2();
    }
}

void Screen1(){
    char buffer[21];
    char buf[8], buf2[8];
    lcd.home();
    lcd.clear();
    sprintf(buffer, "--------%s-------", pot.time);
    lcd.print(buffer);

    dtostrf(pot.temperature, 0, 1, buf);
    dtostrf(pot.humidity, 0, 1, buf2);
    sprintf(buffer, "Tem: %s Hum: %s%%", buf, buf2);
    lcd.setCursor(0, 1);
    lcd.print(buffer);

    dtostrf(pot.light, 0, 0, buf);
    sprintf(buffer, "Light: %s lx", buf);
    lcd.setCursor(0, 2);
    lcd.print(buffer);

    dtostrf(pot.pressure, 0, 2, buf);
    sprintf(buffer, "Pres: %s hPa", buf);
    lcd.setCursor(0, 3);
    lcd.print(buffer);
}

void Screen2(){
    //obmedzenie pozície y
    if(pos.y < 1){
        pos.y = 7;
    }else if(pos.y > 7){
        pos.y = 1;
    }
    //ak je tlačidlo ENTER stlačené tak potom zmení dáta
    if(digitalRead(BTN_ENTER)){
        switch(pos.y){
            case 1:
                while(digitalRead(BTN_ENTER));
                pot.set.autoWatering = !pot.set.autoWatering;
                break;
            case 2:
                while(digitalRead(BTN_ENTER));
                pot.set.allowedWatering = !pot.set.allowedWatering;
                break;
            case 3:
                pot.set.timeWatering = btn(pot.set.timeWatering, 3);
                break;
            case 4:
                pot.set.minM = btn(pot.set.minM, 0);
                break;
            case 5:
                pot.set.maxM = btn(pot.set.maxM, 1);
                break;
            case 6:
                pot.set.minH = btn(pot.set.minH, 2);
                break;
            case 7:
                pot.set.maxH = btn(pot.set.maxH, 4);
        }
    }
    lcd.clear();
    if(pos.y < 4){
        lcd.setCursor(0, 0);
        lcd.print("------Settings------");
        //výpise kurzor
        lcd.setCursor(0, pos.y);
        lcd.print(">");

        lcd.setCursor(1, 1);
        lcd.print("Auto watering:");
        lcd.print(pot.set.autoWatering ? "true " : "false");

        lcd.setCursor(1, 2);
        lcd.print("Allow. water.:");
        lcd.print(pot.set.allowedWatering ? "true " : "false");

        lcd.setCursor(1, 3);
        lcd.print("Time watering: ");
        lcd.print(pot.set.timeWatering);
    }else{
        lcd.setCursor(0, pos.y - 4);
        lcd.print(">");

        lcd.setCursor(1, 0);
        lcd.print("Min moisture:  ");
        lcd.print(pot.set.minM);

        lcd.setCursor(1, 1);
        lcd.print("Max moisture:  ");
        lcd.print(pot.set.maxM);

        lcd.setCursor(1, 2);
        lcd.print("Min humidity:  ");
        lcd.print(pot.set.minH);

        lcd.setCursor(1, 3);
        lcd.print("Max humidity:  ");
        lcd.print(pot.set.maxH);
    }
}

// pomocná funkcia pre Screen2
// dáta budú uložené až po ukončeniu funkcie
int btn(int data, int y){
    while(digitalRead(BTN_ENTER)){
        if(digitalRead(BTN_UP)){
            while(digitalRead(BTN_UP));
                data++;
                lcd.setCursor(16, y);
                lcd.print(data);
                lcd.print(" ");
            }
        if(digitalRead(BTN_DOWN)){
            while(digitalRead(BTN_DOWN));
            data--;
            if(data < 0)
                data = 0;
            lcd.setCursor(16, y);
            lcd.print(data);
            lcd.print(" ");
        }
    }
    return data;
}

void Screen0(){
    char buffer[21];
    char buf[8];
    //obmedzenie pozície y
    if(pos.y < 1){
        pos.y = 3;
    }else if(pos.y > 3){
        pos.y = 1;
    }
    lcd.clear();
    //výpise kurzor
    lcd.setCursor(0, pos.y);
    lcd.print('>');

    lcd.setCursor(0, 0);
    lcd.print(pot.error_message);

    dtostrf(pot.moisture, 0, 0, buf);
    sprintf(buffer, "Moisture: %s %%   ", buf);
    lcd.setCursor(1, 1);
    lcd.print(buffer);

    lcd.setCursor(1, 2);
    lcd.print("Water");

    lcd.setCursor(1, 3);
    lcd.print("Light");

    if(digitalRead(BTN_ENTER)){
        while(digitalRead(BTN_ENTER));
        switch (pos.y){
            case 1:
                pot.moisture = readMoisture(true);
                error();
                Screen0();
                break;
            case 2:
                execute(1);
                break;
            case 3:
                execute(6);
        }
    }
}

void ScreenWat(int i){
    if(i == 0){
        lcd.clear();
        lcd.setCursor(4, 0);
        lcd.print("--Watering--");
        lcd.setCursor(0, 2);
    }
    lcd.print(char(255));
}
