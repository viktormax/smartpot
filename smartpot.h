#ifndef _SMARTPOT_H
#define _SMARTPOT_H

//Pripojené knižnice
#include <Arduino.h>
#include <SparkFunHTU21D.h>
#include <Adafruit_BMP085.h>
#include <BH1750FVI.h>
#include <iarduino_RTC.h>

//Definícia Pinov
#define R_1 A9
#define G_1 A8
#define B_1 A7
#define RELE 45           //Len DIGITÁLNY pin!
#define R_2  4
#define G_2  3
#define B_2  2
#define BTN_ENTER  A6
#define BTN_UP     A5
#define BTN_DOWN   A4
#define BTN_RIGHT  A3
#define BTN_LEFT   A2
#define MOIST_READ A1    //Len ANALÓGOVÝ pin!
#define MOIST_VCC  A0

//Vložiť HIGH keď relé je Low-level, inak LOW
#define SWITCH HIGH

//Pauza medzi obnovami displeja
#define DELAY 10000

//predvolené nastavenia
struct settings{
    bool light = false;
    bool autoWatering = true;
    bool allowedWatering = true;
    int timeWatering = 10;
    int minH = 60;
    int maxH = 90;
    int minM = 40;
    int maxM = 60;
};

struct data{
    struct settings set;
    int last_watering;
    String error_message;
    float temperature;
    uint16_t light;
    float humidity;
    double pressure;
    int moisture;
    char time[6];
};

extern struct data pot;

// funkcia vypíše chybný stav, ak je všetko v poraidku vypíše OK
// a zapne červene LED
void error();

// funkcia vypíše celú śtruktúru pot
void printData();

// funkcia vypíše aktuálny stav nastavení
void printSettings();

// funkcia na rozoznávanie vstupného textu cez bluetooth
int inputBluetooth(String text);

// funkcia na aktualizovanie dát v štruktúre pot
void fillPot(bool mois);

// funkcia na definovanie pinov
void definePins();

// funkcia na načítanie hodnoty zo senzora pôdy, len ak je force true
// v prípade ak je vlhkosť pôdy nízka a od posledného polievania prešla hodina
// a je autoWatering nastavene na true, tak potom pumpa sa automaticky zapne
int readMoisture(bool force);

// funkcia na vykonanie príakzu podľa parametra command
void execute(int command);

// funkcia na výpis príkazov a ich opis
void help();

// funkcia bude čakať definovaný čas, alebo kým nie je stlačené tlačidlo
// alebo nie sú poslane dáta cez bluetooth
void myDelay();

// načíta dáta s Serial a vráti hodnotu
int readInput(); 

#endif
