#include "smartpot.h"

//Funkcia vypíše celú štruktúru pot cez bluetooth
void printData(){
    Serial.print("----------");
    Serial.print(pot.time);
    Serial.println("----------");
    Serial.print("Temperature: ");
    Serial.print(pot.temperature);
    Serial.print(" °C\nLight: ");
    Serial.print(pot.light);
    Serial.print(" lx\nHumidity: ");
    Serial.print(pot.humidity);
    Serial.print(" %\nPressure: ");
    Serial.print(pot.pressure);
    Serial.print(" hPa\nMoisture: ");
    Serial.print(pot.moisture);
    Serial.println(" %");
}

//Funkcia vypíše aktuálne nastavenia cez bluetooth
void printSettings(){
    Serial.println("-------Settings-------");
    Serial.print("Auto watering: ");
    Serial.println(pot.set.autoWatering ? "true" : "false");
    Serial.print("Allowed watering: ");
    Serial.println(pot.set.allowedWatering ? "true" : "false");
    Serial.print("Watering time: ");
    Serial.print(pot.set.timeWatering);
    Serial.println("s");
    Serial.print("Max moisture: ");
    Serial.print(pot.set.maxM);
    Serial.println("%");
    Serial.print("Min moisture: ");
    Serial.print(pot.set.minM);
    Serial.println("%");
    Serial.print("Max humidity: ");
    Serial.print(pot.set.maxH);
    Serial.println("%");
    Serial.print("Min humidity: ");
    Serial.print(pot.set.minH);
    Serial.println("%");
}

//Funkcia na rozoznávanie vstupného textu cez bluetooth
//@parm rozoznávaný text
//@return inštrukčný kód na vykonanie
int inputBluetooth(String text) {
    if (text == "water" || text == "water\n")
        return 1;
    if (text == "print" || text == "print\n")
        return 2;
    if (text == "watering true" || text == "watering true\n")
        return 3;
    if (text == "watering false" || text == "watering false\n")
        return 4;
    if (text == "print settings" || text == "print settings\n")
        return 5;
    if (text == "light" || text == "light\n")
        return 6;
    if (text == "error" || text == "error\n")
        return 7;
    if (text == "auto watering true" || text == "auto watering true\n")
        return 8;
    if (text == "auto watering false" || text == "auto watering false\n")
        return 9;
    if (text == "help" || text == "help\n")
        return 10;
    if (text == "set min moisture" || text == "set min moisture\n")
        return 11;
    if (text == "set max moisture" || text == "set max moisture\n")
        return 12;
    if (text == "set min humidity" || text == "set min humidity\n")
        return 13;
    if (text == "set max humidity" || text == "set max humidity\n")
        return 14;
    if (text == "set time watering" || text == "set time watering\n")
        return 15;    
    return -1;
}

int readInput(){
    Serial.print("Put data: ");
    while(!Serial.available());
    return Serial.parseInt();
}

// funkcia na výpis príkazov a ich opis
void help(){
    Serial.println("> water - príkaz na okamžité polievanie");
    Serial.println("> print - príkaz na výpis štruktúry pot");
    Serial.println("> watering true - zapnutie manuálneho polievania");
    Serial.println("> watering false - vypnutie manuálneho polievania");
    Serial.println("> print settings - vypísanie aktuálneho stavu nastavení");
    Serial.println("> light - príkaz na zapnuti/vypnutie svetla");
    Serial.println("> error - výpis chýb, pri žadnej chybe vypíše OK");
    Serial.println("> auto watering true - zapne automatické polievanie");
    Serial.println("> auto watering false - vypne automatické polievanie");
    Serial.println("> set max|max humidity|moisture - nastaví \nhodnotu premennej");
    Serial.println("> set time watering - nastaví cas");
}
