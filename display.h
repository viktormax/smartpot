#ifndef _DISPLAY_H
#define _DISPLAY_H

#include <LiquidCrystal_I2C.h>

// Inicializuje display
void lcd_begin();

// Pomocná funkcia pre Screen2
// Zvýši alebo zníži dáta podľa stlačených tlačidiel
int btn(int data, int y);

// Nastaví hodnotu x a y
// Rozhoduje aký display vypíše
void lcd_print();

// Zobrazí screen (display2)
void Screen2();

// Zobrazí screen (display1)
void Screen1();

// Zobrazí screen (display0)
void Screen0();

 // Vypisuje priebeh zavlažovania
void ScreenWat(int i);

#endif
