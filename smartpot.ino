#include "smartpot.h"
#include "display.h"

//Inicialíza globálnych premenných
iarduino_RTC time(RTC_DS3231);
BH1750FVI LightSensor;
Adafruit_BMP085 Bmp;
HTU21D Humidity;
struct data pot;

void setup() {
    //Nastavenie rýchlosti prenášania údajov cez serial, v bitoch za sekundu
    Serial.begin(9600);

    //Inicialíza senzorov
    LightSensor.begin();
    Humidity.begin();
    Bmp.begin();
    time.begin();

    //Inicialíza displeja
    lcd_begin();

    //Pre potrebu zmeniena času, použite túto funkciu:
    //format: seconds, minutes, hour, day, month, year, weekday (0 if Sunday 6 if Saturday)
    //time.settime(00,01,10,28,10,18,0);

    //Nastavenia snímača svetla
    LightSensor.SetAddress(0x23);  //Sem napísať adresu modulu. V tomto prípade 0x23
    LightSensor.SetMode(Continuous_H_resolution_Mode);

    //Deklarácia pinov
    definePins();
    //Vypnutie relé
    digitalWrite(RELE, SWITCH);
    //Obnova dát
    fillPot(true);
}


void loop() {
    //Obnova dát
    fillPot(false);
    //Generuje chybu
    error();
    //Výpis dat na displej
    lcd_print();
    //Počká stanovený čas. Keď je tlačidlo stlačené - exit
    myDelay();
}

// zisti čí došlo k chybe
// a zapne červene LED
void error(){
    if(pot.set.minH > pot.humidity){
        pot.error_message = "State: Low humidity ";
    }else if(pot.set.maxH < pot.humidity){
        pot.error_message = "State: High humidity";
    }else if(pot.set.minM > pot.moisture){
        pot.error_message = "State: Low moisture ";
    }else if(pot.set.maxM < pot.moisture){
        pot.error_message = "State: High moisture";
    }else{
        if(!pot.set.light){
          digitalWrite(R_1, LOW);
          digitalWrite(R_2, LOW);
        }
        pot.error_message = "State: OK           " ;
        return;
    }
    //vypne LED (ak je zapnute) okrem červenej farby
    digitalWrite(R_1, HIGH);
    digitalWrite(R_2, HIGH);
    digitalWrite(B_1, LOW);
    digitalWrite(B_2, LOW);
    digitalWrite(G_1, LOW);
    digitalWrite(G_2, LOW);
}

//zapne LED
void light(bool state){
    digitalWrite(R_1, state);
    digitalWrite(R_2, state);
    digitalWrite(B_1, state);
    digitalWrite(B_2, state);
    digitalWrite(G_1, state);
    digitalWrite(G_2, state);
}

// funkcia na aktualizovanie dát v štruktúre pot
void fillPot(bool forse) {
    time.gettime();
    sprintf(pot.time, "%02d:%02d", time.Hours, time.minutes);
    pot.moisture = readMoisture(forse);
    pot.light = LightSensor.GetLightIntensity();
    pot.humidity = Humidity.readHumidity();
    pot.temperature = Humidity.readTemperature();
    pot.pressure = Bmp.readPressure()/100.0;
}

void execute(int command) {
    switch (command) {
        case 1:  //water - príkaz na okamžité polievanie
            if(pot.set.allowedWatering){
                digitalWrite(RELE, !SWITCH);
                Serial.print("Watering: ");
                for(int i = 0; i < 20; i++){
                    ScreenWat(i);
                    if((i % 2) == 0) Serial.print('#');
                    delay(pot.set.timeWatering*100/2);
                }
                digitalWrite(RELE, SWITCH);
                Serial.println();
                fillPot(true);
                lcd_print();
            }
            break;
        case 2:  //print - príkaz na výpis štruktúry pot
            fillPot(true);
            printData();
            break;
        case 3:  //watering true - príkaz na zapnutie manuálneho polievania
            pot.set.allowedWatering = true;
            break;
        case 4:  //watering false - príkaz na vypnutie manuálneho polievania
            pot.set.allowedWatering = false;
            break;
        case 5:  //watering false - príkaz na vypnutie manuálneho polievania
            printSettings();
            break;
        case 6:  //light - príkaz na zapnuti/vypnutie svetla
            pot.set.light = !pot.set.light;
            light(pot.set.light);
            error();
            break;
        case 7:  //error - príkaz na výpis chýb, prižadnej chybe vypíše OK
            error();
            Serial.println(pot.error_message);
            break;
        case 8:  //auto watering true -> príkaz na zapnutie automatického polievania
            pot.set.autoWatering = true;
            break;
        case 9:  //auto watering false -> príkaz na vypnutie automatického polievania
            pot.set.autoWatering = false;
            break;
        case 10:  //help - príkaz na výpis všetkých príkazov a ich popis
            help();
            break;
        case 11: //ďalšie commady nastavia hodnoty premenných
            pot.set.minM = readInput();
            break;
        case 12:
            pot.set.maxM = readInput();
            break;
        case 13:
            pot.set.minH = readInput();
            break;
        case 14:
            pot.set.maxH = readInput();
            break;
        case 15:
            pot.set.timeWatering = readInput();
            break;   
    }
}

// funkcia bude čakať definovaný čas, alebo kým nie je stlačené tlačidlo
// alebo nie sú poslane dáta cez bluetooth
void myDelay(){
    int i = 0;
    while(i < DELAY){
        if(Serial.available()){
            execute(inputBluetooth(Serial.readString()));  //vykoná príkaz
            break;
        }
        if(digitalRead(BTN_UP) || digitalRead(BTN_DOWN) || digitalRead(BTN_ENTER))
            break;
        if(digitalRead(BTN_LEFT) || digitalRead(BTN_RIGHT))
            break;
        delay(1);
        i++;
    }
}

// funkcia na načítanie hodnoty zo senzora pôdy, len ak je force true
// v prípade ak je vlhkosť pôdy nízka a od posledného polievania prešla hodina
// a je autoWatering nastavene na true, tak potom pumpa sa automaticky zapne
int readMoisture(bool force){
    int d = pot.moisture;
    //načíta dáta každé 10 minúť alebo force je true
    if((time.minutes % 10 == 0) || force){
        digitalWrite(MOIST_VCC, HIGH);  //zapne senzor
        delay(10); //počká na inicializáciu
        d = map(analogRead(MOIST_READ), 100, 900, 100, 0);
        digitalWrite(MOIST_VCC, LOW);   //vypne senzor
    }
    //zapne pumpu
    if(time.Hours !=  pot.last_watering && pot.set.autoWatering){
        if(d < pot.set.minM){
            pot.last_watering = time.Hours;
            execute(1);
        }
    }
    return d;
}

void definePins(){
    pinMode(RELE, OUTPUT);
    pinMode(R_1, OUTPUT);
    pinMode(G_1, OUTPUT);
    pinMode(B_1, OUTPUT);
    pinMode(R_2, OUTPUT);
    pinMode(G_2, OUTPUT);
    pinMode(B_2, OUTPUT);
    pinMode(BTN_ENTER, INPUT);
    pinMode(BTN_UP, INPUT);
    pinMode(BTN_DOWN, INPUT);
    pinMode(BTN_RIGHT, INPUT);
    pinMode(BTN_LEFT, INPUT);
    pinMode(MOIST_READ, INPUT);
    pinMode(MOIST_VCC, OUTPUT);
}
